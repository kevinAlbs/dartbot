/* 
   Simple program which recieves serial communication and changes servo speed accordingly
   Expects communication to be the speed as an integer followed by \n
   The speed ranges from 0 to 180 with 90 being a full stop
   The servo is on pwm pin 9
 */
 
#include <Servo.h>
#include <stdlib.h>


Servo xServo;
Servo yServo;
Servo tServo;

String strX = "";         //string which gets x speed from serial
String strY = "";
String strT = ""; 

boolean strXComplete = false;  // whether the string x is complete
boolean strYComplete = false;
boolean strTComplete = false;

int led = 13;
int xSpeed = 90;
int yPos = 90;
int tSpeed = 90;

void setup() {
	pinMode(led, OUTPUT);
	// initialize serial:
	Serial.begin(9600);
	// reserve 200 bytes for the inputString:
	strX.reserve(200);
        strY.reserve(200);
        strT.reserve(200);
        
	xServo.attach(9);
        yServo.attach(10);
        yServo.write(180);
        tServo.attach(11);
}

void loop() {
	// print the string when a newline arrives:
	if (strXComplete && strYComplete && strTComplete) {

		char buffer[10];
		strX.toCharArray(buffer, 10);
		xSpeed = atoi(buffer);

		strY.toCharArray(buffer, 10);
                yPos = atoi(buffer);

                strT.toCharArray(buffer, 10);
                tSpeed = atoi(buffer);
                
		digitalWrite(led, HIGH);
		if(xSpeed >= 70 && xSpeed <= 120){
          		xServo.write(xSpeed);
                }
                delay(15);
                if(yPos >= 0 && yPos <= 180){
                  yServo.write(yPos);
                }
                delay(15);
                if(tSpeed >= 0 && tSpeed <= 180){
                  tServo.write(tSpeed);
                }
                
		// clear the string:
		strX = "";
                strY = "";
                strT = "";
		strXComplete = false;
                strYComplete = false;
                strTComplete = false;
	}
        //SoftwareServo::refresh(); //Arduino docs told me to do this, idk why
}

/*
   SerialEvent occurs whenever a new data comes in the
   hardware serial RX.  This routine is run between each
   time loop() runs, so using delay inside loop can delay
   response.  Multiple bytes of data may be available.
 */
 /*
 Serial data for speed should be entered as such:
 <xspeed>_<yspeed>\n
 */
void serialEvent() {
	while (Serial.available()) {
		// get the new byte:
		char inChar = (char)Serial.read(); 
		// add it to the inputString:

		// if the incoming character is a newline, set a flag
		// so the main loop can do something about it:
		//do not append to inputString
		if (inChar == '\n') {
			strTComplete = true;
		} 
                else if(inChar == '_'){
                        if(!strXComplete){
                          strXComplete = true;
                        }
                        else{
                          strYComplete = true;
                        }
                }
		else{
			if(!strXComplete){
                          strX += inChar;
                        }
                        else if(!strYComplete){
                          strY += inChar;
                        }
                        else if(!strTComplete){
                          strT += inChar;
                        }
                }
	}
}


