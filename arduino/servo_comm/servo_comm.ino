/* 
   Simple program which recieves serial communication and changes servo speed accordingly
   Expects communication to be the speed as an integer followed by \n
   The speed ranges from 0 to 180 with 90 being a full stop
   The servo is on pwm pin 9
 */
 
#include <Servo.h>
#include <stdlib.h>

Servo myservo;
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int led = 13;
int servoSpeed = 90;

void setup() {
	pinMode(led, OUTPUT);
	// initialize serial:
	Serial.begin(9600);
	// reserve 200 bytes for the inputString:
	inputString.reserve(200);
	myservo.attach(11);
}

void loop() {
	// print the string when a newline arrives:
	if (stringComplete) {

		char buffer[10];
		inputString.toCharArray(buffer, 10);
		servoSpeed = atoi(buffer);
		// Serial.println(buffer);
		Serial.print(servoSpeed);
		digitalWrite(led, HIGH); 
      //          if(servoSpeed < 120 && servoSpeed > 70){
		myservo.write(servoSpeed);
        //        }
		digitalWrite(led, LOW);
		// clear the string:
		inputString = "";
		stringComplete = false;
	}
      //delay(15);
}

/*
   SerialEvent occurs whenever a new data comes in the
   hardware serial RX.  This routine is run between each
   time loop() runs, so using delay inside loop can delay
   response.  Multiple bytes of data may be available.
 */
void serialEvent() {
	while (Serial.available()) {
		// get the new byte:
		char inChar = (char)Serial.read(); 
		// add it to the inputString:

		// if the incoming character is a newline, set a flag
		// so the main loop can do something about it:
		//do not append to inputString
		if (inChar == '\n') {
			stringComplete = true;
		} 
		else{
			inputString += inChar;
		}
	}
}


