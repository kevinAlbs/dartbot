from SimpleCV import *
import sys
import time
import serial

cam = Camera()
disp = Display()
if len(sys.argv) < 2:
	print "Please provide name of arduino in /dev"
	sys.exit()
#Get device name of arduino
arduinoDev = sys.argv[1]
ser = serial.Serial("/dev/" + arduinoDev, 9600)
#Wait for serial connection (there's gotta be a better way than this)
time.sleep(2)
print("Testing movement...\n")
print("Moving right\n");
ser.write("100\n")
time.sleep(1)
print("Stop\n")
ser.write("90\n")
time.sleep(1)
print("Moving left\n")
ser.write("85\n")
time.sleep(1)
ser.write("Stop\n");
ser.write("90\n")


while disp.isNotDone():
    img = cam.getImage()
    if disp.mouseLeft:
        break
    mask = img.createBinaryMask(color1=(128,0,0),color2=(255,64,64))
    blobs = img.findBlobsFromMask(mask)
    avgCentroidX = 0
    avgCentroidY = 0
    if blobs is not None:
        blobCount = len(blobs)
        print blobs
        for b in blobs:
           	(x1, y1) = b.centroid()
           	avgCentroidX += x1
	        avgCentroidY += y1
		avgCentroidX /= blobCount
        avgCentroidY /= blobCount
    img.drawCircle((avgCentroidX, avgCentroidY),4,color=(0,255,0))
    img.drawCircle((img.width/2,img.height/2),3,color=(0,255,0))
    img.save(disp)

