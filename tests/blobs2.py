from SimpleCV import *
calibrate = (70, 70)
def getUpperLeft(blobs):
    x = min(blobs, lambda x,y: x.minX() > y.minX()).minX()
    y = min(blobs, lambda x,y: x.minY() > y.minY()).minY()
    return(x,y)
def getLowerRight(blobs):
    x = max(blobs, lambda x,y: x.maxX() > y.maxX()).maxX()
    y = max(blobs, lambda x,y: x.maxY() > y.maxY()).maxY()
    return (x,y)
def findCenter(b):
    if b != null:
        return (b.minX()+b.width()/2,b.minY()+b.height()/2)
    else:
        return (-1,-1)

def getError(blobs):
    p = max(blobs,lambda x,y: x.area() > y.area())
    return p.centroid() - calibrate
    

cam = Camera()
disp = Display()

while disp.isNotDone():
    img = cam.getImage()
    if disp.mouseLeft:
        break
    mask = img.createBinaryMask(color1=(140,0,0),color2=(255,105,105))
    blobs = img.findBlobsFromMask(mask)
    if blobs is not None:
        print blobs
        ul = getUpperLeft(blobs)
        lr = getLowerRight(blobs)
        (x1, y1) = ul
        (x2, y2) = lr
        img.drawRectangle(x1, y1, x2-x1, y2-y1 ,color=(0,255,0))
    img.drawCircle((img.width/2,img.height/2),3,color=(0,255,0))
    img.save(disp)
