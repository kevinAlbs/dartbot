import serial

idle_amt = 90 #Default for zero speed
servoX = None #Base servo (continuous)
servoY = None #Tilt servo (180 deg)
servoT = None #Trigger servo (continous)
ser = None
moving = False
curSpeed = 0

def init(arduinoDev):
    global ser
    ser = serial.Serial("/dev/" + arduinoDev, 9600)
    
def move_x(amt):
    global moving, curSpeed, idle_amt
    if(amt == curSpeed):
        return
    diff = idle_amt + amt
    if(amt != 0):
        moving = True
    else:
        moving = False
    curSpeed = amt
    ser.write(str(diff) + "\n")

