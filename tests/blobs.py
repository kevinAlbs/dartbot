from SimpleCV import *
cam = Camera(1)
disp = Display()

while disp.isNotDone():
    img = cam.getImage()
    if disp.mouseLeft:
        break
    mask = img.createBinaryMask(color1=(128,0,0),color2=(255,99,99))
    blobs = img.findBlobsFromMask(mask)
    if blobs is not None:
        print blobs
        for b in blobs:
            img.drawRectangle(b.minX(),b.minY(),b.width(),b.height(),color=(0,255,0))
    img.drawCircle((img.width/2,img.height/2),3,color=(0,255,0))
    img.save(disp)
