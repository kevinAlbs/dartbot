from SimpleCV import *
import sys
import time
import servo

cam = Camera(1)
disp = Display()
threshold_x = 50 #Threshold for base rotation
threshold_y = 50
crosshair_x = -1
crosshair_y = -1

if len(sys.argv) < 2:
	print "Please provide name of arduino in /dev"
	sys.exit()
#Get device name of arduino
arduinoDev = sys.argv[1]
servo.init(arduinoDev)
#Wait for serial connection (there's gotta be a better way than this)
time.sleep(2)

while disp.isNotDone():
    img = cam.getImage()
    if disp.mouseLeft:
        break
    mask = img.createBinaryMask(color1=(128,0,0),color2=(255,99,99))
    blobs = img.findBlobsFromMask(mask)
    largest_blob = None
    if blobs is not None:
        blobCount = len(blobs)
        for b in blobs:
            if largest_blob is None:
                largest_blob = b
            elif b.area() > largest_blob.area():
                largest_blob = b
    if(largest_blob is not None):
        img.drawRectangle(largest_blob.minX(),largest_blob.minY(),largest_blob.width(),largest_blob.height(),color=(0,255,0))
        centerx = largest_blob.minX() + (largest_blob.width() / 2)
        centery = largest_blob.minY() + (largest_blob.height()/ 2)
        crosshair_x = img.width/2
        crosshair_y = img.height/2
        #Find out how much to rotate base
        diff_x = crosshair_x - centerx
        abs_diff = math.fabs(diff_x)
        if(abs_diff > threshold_x and not servo.moving):
            #move
            speed = -5 if (diff_x > 0) else 10 
            servo.move_x(speed)
        elif(abs_diff < threshold_x and servo.moving):
            servo.move_x(0)
                
        img.drawCircle((centerx, centery), 5, color=(0,0,255))
    elif(servo.moving):
        servo.move_x(0)

    img.drawCircle((crosshair_x, crosshair_y),3,color=(0,255,0))
    img.save(disp)
