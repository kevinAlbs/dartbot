import serial
import time

ser = None
moving = False
curSpeedX = 0
curPosY = 180

kpx = .2
kix = 0.0
kdx = 0.1
	
kpy = 0.2
kiy = 0.0
kdy = 0.3

idle_x = 92
idle_y = curPosY

ix = 0

ix= 0 
iy=0 
dx=0 
dy=0
prevx=0
prevy=0

def init(arduinoDev):
    global ser
    ser = serial.Serial("/dev/" + arduinoDev, 9600)

def constrain(x,a,b):
	if x < a:
		return a
	elif x > b:
		return b
	else:
		return x

def stop():
	global curPosY
	ser.write('90_' + str(curPosY) + '_90\n')
	print("Stop!")

def fire():
	ser.write("90_" + str(curPosY) + "_60\n");
	print("Firing")
	time.sleep(1)
	print("Returning")
	ser.write("90_" + str(curPosY) + "_100\n");
	time.sleep(1.5)

#arguments: x and y pixel distances from target to center in camera image
def move(ex,ey):
	global ix, iy, dx, dy, prevx, prevy
	global kpx,kpy,kix,kiy,kdx,kdy
	global idle_x, idle_y
	global curPosY, moving
	
	ix = ix + ex
	iy = iy + ey
	
	dx = ex - prevx
	dy = ey - prevy
	prevx = ex;
	prevy = ey;

	ux = kpx*ex + kix*ix + kdx*dx
	uy = kpy*ey + kiy*iy + kdy*dy
	
	ux = constrain(ux,-9,7)
	uy = constrain(uy,-1,1)
	ux = ux * -1
	uy = uy * -1
	outx = round(idle_x + ux)
	outy = round(curPosY + uy)
	curPosY = outy
	outy = constrain(outy, 0, idle_y)
	
	print("UY %d" % uy);
	serStr = "%d_%d_90\n" % (outx, outy)
	print(serStr)
	if(ux == 0 and not moving):
		#ignore
		moving = False
	else:
		ser.write(serStr)

	if(ux == 0):
		moving = False
	else:
		moving = True
	
