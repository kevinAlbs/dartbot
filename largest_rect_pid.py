from SimpleCV import *
import sys
import time
from time import sleep
import pid

def getTime():
    return int(round(time.time() * 1000))

cam = Camera(1, {"width" : 160, "height" : 120})
disp = Display()
threshold_x = 50 #Threshold for base rotation
threshold_y = 50
crosshair_x = -1
crosshair_y = -1
prevTime = 0



if len(sys.argv) < 2:
	print "Please provide name of arduino in /dev"
	sys.exit()
#Get device name of arduino
arduinoDev = sys.argv[1]
#servo.init(arduinoDev)
pid.init(arduinoDev)
#Wait for serial connection (there's gotta be a better way than this)
time.sleep(2)

prevTime = getTime()
while disp.isNotDone():
    sleep(.01)
    img = cam.getImage()
    if disp.mouseLeft:
        break
    mask = img.createBinaryMask(color1=(140,0,0),color2=(255,105,105))
    blobs = img.findBlobsFromMask(mask, appx_level=10, minsize=20)
    largest_blob = None
    blobCount = 0
    if blobs is not None:
        for b in blobs:
            blobCount += 1
            if largest_blob is None:
                largest_blob = b
            elif b.area() > largest_blob.area():
                largest_blob = b
    if(largest_blob is not None):
        img.drawRectangle(largest_blob.minX(),largest_blob.minY(),largest_blob.width(),largest_blob.height(),color=(0,255,0))
        centerx = largest_blob.minX() + (largest_blob.width() / 2)
        centery = largest_blob.minY() + (largest_blob.height()/ 2)
        crosshair_x = img.width/2
        crosshair_y = img.height/2
        #Find out how much to rotate base
        diff_x = crosshair_x - centerx
        diff_y = crosshair_y - centery
        if(diff_x**2 + diff_y**2 < 150):
            pid.stop()
            print('fire!\a')
        else:
            pid.move(diff_x,diff_y)
        img.drawCircle((centerx, centery), 5, color=(0,0,255))
    else:
        pid.stop()
    #Check to see if time difference is too long
    curTime = getTime()
    if(curTime - prevTime > 400):
        pid.stop()
        sys.exit() #suicide
    img.drawCircle((crosshair_x, crosshair_y),3,color=(0,255,0))
    img.save(disp)
    prevTime = getTime()
